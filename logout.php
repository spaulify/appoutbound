<?php /*! Logout of this Application */

require_once(dirname(__FILE__) . '/config/config.php');

$cookie_time = (3600 * 24 * 30);
setcookie('__ssUn', '', time() - $cookie_time, '/');
setcookie('__ssPs', '', time() - $cookie_time, '/');

$_SESSION['Cust_ID'] = '';
unset($_SESSION['Cust_ID']);


header('location:' . SERVER1_URL . "welcome/");

exit;