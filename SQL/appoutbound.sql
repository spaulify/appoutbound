-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2017 at 02:28 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appoutbound`
--

-- --------------------------------------------------------

--
-- Table structure for table `appob_accesscentre`
--

CREATE TABLE `appob_accesscentre` (
  `Access_ID` int(11) NOT NULL,
  `Cust_ID` int(11) NOT NULL,
  `Access_Email_Tool` enum('NO','YES') NOT NULL DEFAULT 'NO',
  `Access_Download` enum('NO','YES') NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `appob_ama`
--

CREATE TABLE `appob_ama` (
  `AMA_ID` int(11) NOT NULL,
  `Cust_ID` int(11) NOT NULL,
  `AMA_Text` varchar(255) NOT NULL,
  `AMA_Text_Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `appob_customer`
--

CREATE TABLE `appob_customer` (
  `Cust_ID` int(11) NOT NULL,
  `Cust_FirstName` varchar(255) NOT NULL,
  `Cust_Lastname` varchar(255) NOT NULL,
  `Cust_Email` varchar(255) NOT NULL,
  `Cust_Website` varchar(255) NOT NULL,
  `Cust_BusinessIdea` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `appob_webinar`
--

CREATE TABLE `appob_webinar` (
  `Webinar_ID` int(11) NOT NULL,
  `Cust_ID` int(11) NOT NULL,
  `Webinar_Name` varchar(255) NOT NULL,
  `Webinar_DateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appob_accesscentre`
--
ALTER TABLE `appob_accesscentre`
  ADD PRIMARY KEY (`Access_ID`);

--
-- Indexes for table `appob_ama`
--
ALTER TABLE `appob_ama`
  ADD PRIMARY KEY (`AMA_ID`);

--
-- Indexes for table `appob_customer`
--
ALTER TABLE `appob_customer`
  ADD PRIMARY KEY (`Cust_ID`);

--
-- Indexes for table `appob_webinar`
--
ALTER TABLE `appob_webinar`
  ADD PRIMARY KEY (`Webinar_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appob_accesscentre`
--
ALTER TABLE `appob_accesscentre`
  MODIFY `Access_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `appob_ama`
--
ALTER TABLE `appob_ama`
  MODIFY `AMA_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `appob_customer`
--
ALTER TABLE `appob_customer`
  MODIFY `Cust_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `appob_webinar`
--
ALTER TABLE `appob_webinar`
  MODIFY `Webinar_ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
